import "./App.css";

import React, { useState, useMemo } from 'react';

function App() {
	const [ text, setText ] = useState("");
	
	const inputHandler = () => {
		const input = document.querySelector('input.input.is-large');
		return setText(input.value);
	}
	
	const state = useMemo(() => {
			var onlyNumbers = /^[0-9]*$/;
			
			if(text === ""){
				if(document.querySelector('i')){
					document.querySelector('i').className = 'fas fa-times';
				}
			}
			
			if(!text.match(onlyNumbers)){
				if(document.querySelector('i') !== null){
					document.querySelector('i').className = 'fas fa-times';
				}
			} else {
				if(document.querySelector('i') !== null){
					document.querySelector('i').className = 'fas fa-check';
				}
			}
		}, [text]);
		
  return (
    <div className="App">
     <div className="control has-icons-right">
        <input
          className="input is-large"
          type="text"
          placeholder="Enter number..."
		  onChange={inputHandler}
        />
        <span className="icon is-small is-right">
          <i className="fas fa-times" />
        </span> 
      </div>
    </div>
  );
}

export default App;
